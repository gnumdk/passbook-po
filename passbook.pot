# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-10-05 17:29+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../data/org.gnome.Passbook.gschema.xml:6
msgid "Minimum sidebar position"
msgstr ""

#: ../data/org.gnome.Passbook.gschema.xml:11
msgid "Window size"
msgstr ""

#: ../data/org.gnome.Passbook.gschema.xml:12
msgid "Window size (width and height)"
msgstr ""

#: ../data/org.gnome.Passbook.gschema.xml:16
msgid "Window position"
msgstr ""

#: ../data/org.gnome.Passbook.gschema.xml:17
msgid "Window position (x and y)"
msgstr ""

#: ../data/org.gnome.Passbook.gschema.xml:21
msgid "Window maximized"
msgstr ""

#: ../data/org.gnome.Passbook.gschema.xml:22
msgid "Window maximized state"
msgstr ""

#: ../data/org.gnome.Passbook.gschema.xml:26
msgid "Use colors"
msgstr ""

#: ../data/org.gnome.Passbook.gschema.xml:31
msgid "Per application color"
msgstr ""

#: ../data/org.gnome.Passbook.gschema.xml:36
msgid "Keepass URIs"
msgstr ""

#: ../data/AboutDialog.ui.in:14
msgid "A password manager for GNOME."
msgstr ""

#: ../data/AboutDialog.ui.in:16
msgid "Visit website"
msgstr ""

#. Replace me with your names
#: ../data/AboutDialog.ui.in:19
msgctxt "Translation credits here, put your name here!"
msgid "Cédric Bellegarde <cedric.bellegarde@adishatz.org>"
msgstr ""

#: ../data/AboutDialog.ui.in:54
msgid "Donations:"
msgstr ""

#: ../data/AboutDialog.ui.in:63
msgid "Liberapay"
msgstr ""

#: ../data/AboutDialog.ui.in:77
msgid "PayPal"
msgstr ""

#: ../data/org.gnome.Passbook.appdata.xml.in:7
#: ../data/org.gnome.Passbook.desktop.in:3
#: ../data/org.gnome.Passbook.desktop.in:4 ../data/MainWindow.ui:49
msgid "Passbook"
msgstr ""

#: ../data/org.gnome.Passbook.appdata.xml.in:8
msgid "Password manager for GNOME"
msgstr ""

#: ../data/org.gnome.Passbook.appdata.xml.in:10
msgid "Passbook is a modern password manager for GNOME."
msgstr ""

#: ../data/org.gnome.Passbook.appdata.xml.in:11
msgid "Features:"
msgstr ""

#: ../data/org.gnome.Passbook.appdata.xml.in:13
msgid "Manages application passwords"
msgstr ""

#: ../data/org.gnome.Passbook.appdata.xml.in:14
msgid "Manager user passwords"
msgstr ""

#: ../data/org.gnome.Passbook.appdata.xml.in:21
msgid "Unreleased"
msgstr ""

#: ../data/org.gnome.Passbook.desktop.in:5
msgid "Manage saved passwords"
msgstr ""

#: ../data/org.gnome.Passbook.desktop.in:7
msgid "dialog-password"
msgstr ""

#: ../data/AddCollectionDialog.ui:21
msgid "Add a new collection"
msgstr ""

#: ../data/AddCollectionDialog.ui:24 ../data/AddPasswordDialog.ui:27
#: ../data/UnlockDialog.ui:19 ../passbook/dialog_add_collection.py:108
msgid "Cancel"
msgstr ""

#: ../data/AddCollectionDialog.ui:33 ../data/AddPasswordDialog.ui:36
#: ../passbook/dialog_add_collection.py:107
msgid "Create"
msgstr ""

#: ../data/AddCollectionDialog.ui:95
msgid ""
"Use collections to group related passwords. You can choose to protect your "
"collection with a password."
msgstr ""

#: ../data/AddCollectionDialog.ui:113
msgid "Collection name :"
msgstr ""

#: ../data/AddCollectionDialog.ui:143
msgid "Keepass file :"
msgstr ""

#: ../data/AddCollectionDialog.ui:165
msgid "Add a Keepass file"
msgstr ""

#: ../data/AddCollectionDialog.ui:208
msgid ""
"<big>Passbook only provides basic Keepass support, if you want a software "
"dedicated to Keepass: <a href=\"https://gitlab.gnome.org/World/PasswordSafe"
"\">Password Safe</a></big>"
msgstr ""

#: ../data/AddCollectionDialog.ui:221
msgid "OK"
msgstr ""

#: ../data/AddPasswordDialog.ui:24
msgid "Add a new password"
msgstr ""

#: ../data/AddPasswordDialog.ui:92 ../data/SecretsListBoxRow.ui:32
msgid "Username :"
msgstr ""

#: ../data/AddPasswordDialog.ui:104 ../data/SecretsListBoxRow.ui:60
msgid "Password :"
msgstr ""

#: ../data/AddPasswordDialog.ui:132
msgid "Description :"
msgstr ""

#: ../data/AddPasswordDialog.ui:155
msgid "Collection :"
msgstr ""

#: ../data/AddPasswordDialog.ui:178
msgid "Random password"
msgstr ""

#: ../data/AddPasswordDialog.ui:215 ../data/SecretsListBoxRow.ui:122
msgid "Group :"
msgstr ""

#: ../data/AddPasswordDialog.ui:225
msgid ""
"Examples:\n"
"Servers\n"
"Servers/Linux\n"
"Servers/Windows"
msgstr ""

#: ../data/Appmenu.ui:7
msgid "_Add a new collection"
msgstr ""

#: ../data/Appmenu.ui:11
msgid "_Enable colors"
msgstr ""

#: ../data/Appmenu.ui:17
msgid "_Keyboard Shortcuts"
msgstr ""

#: ../data/Appmenu.ui:21
msgid "_About"
msgstr ""

#: ../data/Appmenu.ui:25
msgid "_Quit"
msgstr ""

#: ../data/MainWindow.ui:50
msgid "My passwords"
msgstr ""

#: ../data/MainWindow.ui:205
msgid "Collections"
msgstr ""

#: ../data/MainWindow.ui:257
msgid "Keepass"
msgstr ""

#: ../data/MainWindow.ui:309
msgid "Applications"
msgstr ""

#: ../data/MainWindow.ui:409 ../passbook/pop_groups.py:33
#: ../passbook/window_events.py:106
msgid "All"
msgstr ""

#: ../data/SecretsListBoxRow.ui:87
msgid "Save"
msgstr ""

#: ../data/Shortcuts.ui:13
msgctxt "shortcut window"
msgid "General"
msgstr ""

#: ../data/Shortcuts.ui:17
msgctxt "shortcut window"
msgid "Create collection"
msgstr ""

#: ../data/Shortcuts.ui:24
msgctxt "shortcut window"
msgid "Create password"
msgstr ""

#: ../data/Shortcuts.ui:31
msgctxt "shortcut window"
msgid "Enable colors"
msgstr ""

#: ../data/UnlockDialog.ui:16
msgid "Unlock collection"
msgstr ""

#: ../data/UnlockDialog.ui:28
msgid "Unlock"
msgstr ""

#: ../data/UnlockDialog.ui:112
msgid "Save password"
msgstr ""

#: ../passbook/dialog_add_collection.py:104
msgid "New Keepass file"
msgstr ""

#: ../passbook/dialog_unlock.py:41
msgid "Enter password for this collection :"
msgstr ""

#: ../passbook/dialog_unlock.py:54
#, python-format
msgid "Keepass password for %s"
msgstr ""

#: ../passbook/dialog_unlock.py:122
msgid "Invalid password !"
msgstr ""

#: ../passbook/pop_groups.py:119
msgid "No groups available"
msgstr ""

#: ../passbook/window_content.py:173
msgid ""
"<big>Welcome to <b>Passbook</b>, a password manager helping to keep your "
"information secure on your computer.</big>"
msgstr ""

#: ../passbook/window_content.py:282
msgid "<big>No passwords !</big>"
msgstr ""

#: ../passbook/window_content.py:389
msgid "Others"
msgstr ""

#: ../passbook/window_content.py:446 ../passbook/window_events.py:246
#, python-format
msgid "%s passwords"
msgstr ""

#: ../passbook/window_content.py:448
#, python-format
msgid "%s password"
msgstr ""

#: ../passbook/window_events.py:173
msgid "Delete collection"
msgstr ""

#: ../passbook/window_events.py:175
msgid "Detach collection"
msgstr ""

#: ../passbook/window_events.py:230
#, python-format
msgid "<big>No result for \"%s\"</big>"
msgstr ""
