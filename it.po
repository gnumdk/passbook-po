# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-10-05 17:29+0200\n"
"PO-Revision-Date: 2019-09-04 05:24+0000\n"
"Last-Translator: Swann Martinet <swann.ranskassa@laposte.net>\n"
"Language-Team: Italian <https://hosted.weblate.org/projects/gnumdk/passbook/"
"it/>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.9-dev\n"

#: ../data/org.gnome.Passbook.gschema.xml:6
msgid "Minimum sidebar position"
msgstr "Posizione minima della barra laterale"

#: ../data/org.gnome.Passbook.gschema.xml:11
msgid "Window size"
msgstr "Dimensione della finestra"

#: ../data/org.gnome.Passbook.gschema.xml:12
msgid "Window size (width and height)"
msgstr "Dimensione della finestra (larghezza e altezza)"

#: ../data/org.gnome.Passbook.gschema.xml:16
msgid "Window position"
msgstr "Posizione della finestra"

#: ../data/org.gnome.Passbook.gschema.xml:17
msgid "Window position (x and y)"
msgstr "Posizione della finestra (x e y)"

#: ../data/org.gnome.Passbook.gschema.xml:21
msgid "Window maximized"
msgstr "Finestra massimizzata"

#: ../data/org.gnome.Passbook.gschema.xml:22
msgid "Window maximized state"
msgstr "Stato massimizzato della finstra"

#: ../data/org.gnome.Passbook.gschema.xml:26
msgid "Use colors"
msgstr "Usa colori"

#: ../data/org.gnome.Passbook.gschema.xml:31
msgid "Per application color"
msgstr "Colore per applicazione"

#: ../data/org.gnome.Passbook.gschema.xml:36
msgid "Keepass URIs"
msgstr "URI di Keepass"

#: ../data/AboutDialog.ui.in:14
msgid "A password manager for GNOME."
msgstr "Un gestore di password per GNOME."

#: ../data/AboutDialog.ui.in:16
msgid "Visit website"
msgstr "Visita il sito web"

#. Replace me with your names
#: ../data/AboutDialog.ui.in:19
msgctxt "Translation credits here, put your name here!"
msgid "Cédric Bellegarde <cedric.bellegarde@adishatz.org>"
msgstr "Gianluca Boiano <morf3089@gmail.com>"

#: ../data/AboutDialog.ui.in:54
msgid "Donations:"
msgstr "Donazioni:"

#: ../data/AboutDialog.ui.in:63
msgid "Liberapay"
msgstr "Liberapay"

#: ../data/AboutDialog.ui.in:77
msgid "PayPal"
msgstr "Paypal"

#: ../data/org.gnome.Passbook.appdata.xml.in:7
#: ../data/org.gnome.Passbook.desktop.in:3
#: ../data/org.gnome.Passbook.desktop.in:4 ../data/MainWindow.ui:49
msgid "Passbook"
msgstr "Passbook"

#: ../data/org.gnome.Passbook.appdata.xml.in:8
msgid "Password manager for GNOME"
msgstr "Gestore di password per GNOME"

#: ../data/org.gnome.Passbook.appdata.xml.in:10
msgid "Passbook is a modern password manager for GNOME."
msgstr "Passbook è un moderno gestore di password per GNOME."

#: ../data/org.gnome.Passbook.appdata.xml.in:11
msgid "Features:"
msgstr "Caratteristiche:"

#: ../data/org.gnome.Passbook.appdata.xml.in:13
msgid "Manages application passwords"
msgstr "Gestisce le password elle applicazioni"

#: ../data/org.gnome.Passbook.appdata.xml.in:14
msgid "Manager user passwords"
msgstr "Gestore delle password degli utenti"

#: ../data/org.gnome.Passbook.appdata.xml.in:21
msgid "Unreleased"
msgstr "Non rilasciato"

#: ../data/org.gnome.Passbook.desktop.in:5
msgid "Manage saved passwords"
msgstr "Gestisci password salvate"

#: ../data/org.gnome.Passbook.desktop.in:7
msgid "dialog-password"
msgstr "finestra password"

#: ../data/AddCollectionDialog.ui:21
msgid "Add a new collection"
msgstr "Aggiungi una nuova collezione"

#: ../data/AddCollectionDialog.ui:24 ../data/AddPasswordDialog.ui:27
#: ../data/UnlockDialog.ui:19 ../passbook/dialog_add_collection.py:108
msgid "Cancel"
msgstr "Cancella"

#: ../data/AddCollectionDialog.ui:33 ../data/AddPasswordDialog.ui:36
#: ../passbook/dialog_add_collection.py:107
msgid "Create"
msgstr "Crea"

#: ../data/AddCollectionDialog.ui:95
msgid ""
"Use collections to group related passwords. You can choose to protect your "
"collection with a password."
msgstr ""
"Usa le collezioni per raggruppare le password collegate. Puoi scegliere di "
"proteggere la tua collezione con una password."

#: ../data/AddCollectionDialog.ui:113
msgid "Collection name :"
msgstr "Nome della collezione :"

#: ../data/AddCollectionDialog.ui:143
msgid "Keepass file :"
msgstr "File di Keepass:"

#: ../data/AddCollectionDialog.ui:165
msgid "Add a Keepass file"
msgstr "aggiungi un file Keepass"

#: ../data/AddCollectionDialog.ui:208
msgid ""
"<big>Passbook only provides basic Keepass support, if you want a software "
"dedicated to Keepass: <a href=\"https://gitlab.gnome.org/World/PasswordSafe"
"\">Password Safe</a></big>"
msgstr ""
"<big>Passbook fornisce solo supporto di base per Keepass, se si desidera un "
"software dedicato a Keepass:\n"
"<a href=\"https://gitlab.gnome.org/World/PasswordSafe\">Password "
"Safe</a></big>"

#: ../data/AddCollectionDialog.ui:221
msgid "OK"
msgstr "OK"

#: ../data/AddPasswordDialog.ui:24
msgid "Add a new password"
msgstr "Aggiungi una nuova password"

#: ../data/AddPasswordDialog.ui:92 ../data/SecretsListBoxRow.ui:32
msgid "Username :"
msgstr "Nome utente :"

#: ../data/AddPasswordDialog.ui:104 ../data/SecretsListBoxRow.ui:60
msgid "Password :"
msgstr "Password:"

#: ../data/AddPasswordDialog.ui:132
msgid "Description :"
msgstr "Descrizione :"

#: ../data/AddPasswordDialog.ui:155
msgid "Collection :"
msgstr "Collezione :"

#: ../data/AddPasswordDialog.ui:178
msgid "Random password"
msgstr "Password casuale"

#: ../data/AddPasswordDialog.ui:215 ../data/SecretsListBoxRow.ui:122
msgid "Group :"
msgstr "Gruppo:"

#: ../data/AddPasswordDialog.ui:225
msgid ""
"Examples:\n"
"Servers\n"
"Servers/Linux\n"
"Servers/Windows"
msgstr ""
"Esempi:\n"
"Server\n"
"Server/Linux\n"
"Server/Windows"

#: ../data/Appmenu.ui:7
msgid "_Add a new collection"
msgstr "_Aggiungi una nuova collezione"

#: ../data/Appmenu.ui:11
msgid "_Enable colors"
msgstr "_Attiva i colori"

#: ../data/Appmenu.ui:17
msgid "_Keyboard Shortcuts"
msgstr "Scorciatoie da _tastiera"

#: ../data/Appmenu.ui:21
msgid "_About"
msgstr "_Informazioni"

#: ../data/Appmenu.ui:25
msgid "_Quit"
msgstr "_Esci"

#: ../data/MainWindow.ui:50
msgid "My passwords"
msgstr "Le mie password"

#: ../data/MainWindow.ui:205
msgid "Collections"
msgstr "Collezioni"

#: ../data/MainWindow.ui:257
msgid "Keepass"
msgstr "Keepass"

#: ../data/MainWindow.ui:309
msgid "Applications"
msgstr "Applicazioni"

#: ../data/MainWindow.ui:409 ../passbook/pop_groups.py:33
#: ../passbook/window_events.py:106
msgid "All"
msgstr "Tutti"

#: ../data/SecretsListBoxRow.ui:87
msgid "Save"
msgstr "Salva"

#: ../data/Shortcuts.ui:13
msgctxt "shortcut window"
msgid "General"
msgstr "Generale"

#: ../data/Shortcuts.ui:17
msgctxt "shortcut window"
msgid "Create collection"
msgstr "Crea collezione"

#: ../data/Shortcuts.ui:24
msgctxt "shortcut window"
msgid "Create password"
msgstr "Crea password"

#: ../data/Shortcuts.ui:31
msgctxt "shortcut window"
msgid "Enable colors"
msgstr "Attiva i colori"

#: ../data/UnlockDialog.ui:16
msgid "Unlock collection"
msgstr "Sblocca la raccolta"

#: ../data/UnlockDialog.ui:28
msgid "Unlock"
msgstr "Sblocca"

#: ../data/UnlockDialog.ui:112
msgid "Save password"
msgstr "Salva la password"

#: ../passbook/dialog_add_collection.py:104
msgid "New Keepass file"
msgstr "Nuovo file Keepass"

#: ../passbook/dialog_unlock.py:41
msgid "Enter password for this collection :"
msgstr "Inserisci una password per questa raccolta:"

#: ../passbook/dialog_unlock.py:54
#, python-format
msgid "Keepass password for %s"
msgstr "Password Keepass per %s"

#: ../passbook/dialog_unlock.py:122
msgid "Invalid password !"
msgstr "Password non valida!"

#: ../passbook/pop_groups.py:119
msgid "No groups available"
msgstr "Nessun gruppo disponibile"

#: ../passbook/window_content.py:173
msgid ""
"<big>Welcome to <b>Passbook</b>, a password manager helping to keep your "
"information secure on your computer.</big>"
msgstr ""
"<big>Benvenuti su<b>Passbook</b>, un gestore di password che aiuta a tenere "
"sicure le tue informazioni sul tuo computer.</big>"

#: ../passbook/window_content.py:282
msgid "<big>No passwords !</big>"
msgstr "<big>Nessuna password!</big>"

#: ../passbook/window_content.py:389
msgid "Others"
msgstr "Altri"

#: ../passbook/window_content.py:446 ../passbook/window_events.py:246
#, python-format
msgid "%s passwords"
msgstr "%s password"

#: ../passbook/window_content.py:448
#, python-format
msgid "%s password"
msgstr "%s password"

#: ../passbook/window_events.py:173
msgid "Delete collection"
msgstr "Elimina collezione"

#: ../passbook/window_events.py:175
msgid "Detach collection"
msgstr "Stacca la raccolta"

#: ../passbook/window_events.py:230
#, python-format
msgid "<big>No result for \"%s\"</big>"
msgstr "<big>Nessun risultato per «%s»</big>"

#~ msgctxt "shortcut window"
#~ msgid "Filter passwords"
#~ msgstr "Filtra password"
